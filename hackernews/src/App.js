import React, { Component } from 'react';
import './App.css';

const DEFULT_QUERY = 'redux';
const PATH_BASE = 'https://hn.algolia.com/api/v1';
const PATH_SEARCH = '/search';
const PARAM_SEARCH = 'query=';



class App extends Component {
  // Mounting process has 4 lifecycle methods that are invoked in the following order:
  // constructor, componentWillMount, render, componentDidMount

 // Update lifecycle of a component has 5 methods invoked in the following or der:
 // componentWillReceiveProps, shouldComponentUpdate, componentWillUpdate, render, componentDidUpdate
 

  constructor(props) {
    super(props);
    // add list to internal component state 
    // properties are set for the entire lifespan, while states can be modified.
    // states are accessed by using this.state

    this.state = {
      result: null,
      searchTerm: DEFULT_QUERY,
    };

    this.onDismiss = this.onDismiss.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.setSearchTopstories = this.setSearchTopstories.bind(this);
    this.onSearchSubmit = this.onSearchSubmit(this);
    this.fetchSearchTopstories = this.fetchSearchTopstories.bind(this);
  }


  setSearchTopstories(result){
    this.setState({ result });
  }



  fetchSearchTopstories(searchTerm){
    fetch(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}`)
    .then(res => res.json())
    .then(result => this.setSearchTopstories(result));
  }

  onSearchSubmit(){
    const { searchTerm } = this.state;
    this.fetchSearchTopstories(searchTerm);

  }
  componentDidMount() {
    const { searchTerm } = this.state;
    this.fetchSearchTopstories(searchTerm);
  }

  onDismiss(id) {
    const isNotId = item => item.objectID !== id;
    const updatedHits = this.state.result.hits.filter(isNotId);
    this.setState({
      // when  ... is used every value from an array or object get copied to another array or object
      result: { ...this.state.result, hits: updatedHits}
    });
    }


  onSearchChange(event) {
    this.setState({ searchTerm: event.target.value });
  }


  render() {
    const {searchTerm, result} = this.state;

    return (
      <div className="page">
      <div className="interactions">
        <Search
          value={searchTerm}
          onChange={this.onSearchChange}
          onSubmit={this.onSearchSubmit}
          >
          Search
        </Search>
        </div>
        {result &&
         <Table
          list={result.hits}
          onDismiss={this.onDismiss}
        />
        }
      </div>
    );

  }
}

export default App;


const Search = ({
value,
onChange,
onSubmit,
children
}) =>
<form onSubmit={onSubmit}>
<input
type="text"
value={value}
onChange={onChange}
/>
<button type="submit">
{children}
</button>
</form>

const Table = ({ list, onDismiss }) =>
      <div className="table">
        { list.map(item =>
          <div key={item.objectID} className="table-row">
            <span style={{ width: '40%' }}>
              <a href={item.url} target="_blank">{item.title} </a>
            </span >
            <span style={{ width: '30%'}}>{item.author} </span>
            <span style={{ width: '10%'}}> {item.num_comments} </span>
            <span style={{width: '10%'}}>{item.points}</span>
            <span style={{width: '10%'}}>
              <Button
                onClick={() => onDismiss(item.objectID)}
                className="button-inline"
              >
                Dismiss
              </Button>
            </span>
          </div>
        )}
      </div>


const Button = ({onClick, className, children}) => {

    return(
      <button
        onClick={onClick}
        className={className}
        type="button">
        {children}
      </button>
    );

}